﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recognition
{
    public class math
    {
        public static double EuklidesDistance(double[] wektor1, double[] wektor2)
        {
            Double returnValue = 0.0;

            for (int i = 0; i < wektor1.Length; i++)
            {
                returnValue += Math.Pow(wektor1[i] - wektor2[i], 2);
            }
            return Math.Sqrt(returnValue);
        }

        public static double CityDistance(double[] wektor1, double[] wektor2)
        {
            double returnValue = 0.0;

            for (int i = 0; i < wektor1.Length; i++)
            {
                returnValue += Math.Abs(wektor1[i] - wektor2[i]);
            }
            return returnValue;
        }

    }
}
