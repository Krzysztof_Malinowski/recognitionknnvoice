﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Recognition
{

    class Repository
    {
        static string dataSetPath = "log.json";

        public static List<VoiceData> getAll(string samples)
        {
            var rawData = File.ReadAllText(dataSetPath);

            JObject jObj = (JObject)JsonConvert.DeserializeObject(rawData);

            List<VoiceData> vectorsList = new List<VoiceData>();

            foreach (JObject item in jObj[samples])
            {
                JObject vectorsCount = (JObject)item["arr"];
                List<double[]> list = new List<double[]>();
                for (int i = 1; i <= vectorsCount.Count; i++)
                {
                    JArray array = (JArray)item["arr"][i.ToString()];
                    double[] temp = new double[array.Count];
                    for(int j = 0; j < array.Count; j++)
                    {
                        temp[j] = (double)array[j];
                    }
                    list.Add(temp);
                }

                vectorsList.Add(new VoiceData((String)item["name"], list));
            }
            return vectorsList;
        }

        public static VoiceData getTestVoiceData10()
        {
            List<double[]> list = new List<double[]>();
            list.Add(new Double[] { 0.7779, -0.5474, 0.0037, 0.1931, 0.1517, -0.2407, -0.1131, -0.0358, -0.1879, 0.0616 });
            return new VoiceData("KM", list);
        }

        public static VoiceData getTestVoiceData13()
        {
            List<double[]> list = new List<double[]>();
            list.Add(new Double[] { 0.7779, -0.5474, 0.0037, 0.1931, 0.1517, -0.2407, -0.1131, -0.0358, -0.1879, 0.0616, -0.0594, -0.1068, 0.1569 });
            return new VoiceData("KM", list);
        }

        public static VoiceData getTestVoiceData16()
        {
            List<double[]> list = new List<double[]>();
            list.Add(new Double[] { 0.7779, -0.5474, 0.0037, 0.1931, 0.1517, -0.2407, -0.1131, -0.0358, -0.1879, 0.0616, -0.0594, -0.1068, 0.1569, -0.0594, -0.1068, 0.1569 });
            return new VoiceData("KM", list);
        }
    }
}
