﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recognition
{
    public class KNN
    {
        private int k;

        public struct voice
        {
            public voice(double distance, string person)
            {
                this.person = person;
                this.distance = distance;
            }

            public double distance { get; set; }
            public string person { get; set; }
        }

        public KNN(int k)
        {
            this.k = k;
        }

        public String classification(List<VoiceData> aVoices, VoiceData aVoice,List<String> aNames)
        {
            List<voice> voicesList = getVoices(aVoices, aVoice);

            voicesList = getOrderElements(voicesList);

            Dictionary<string, int> labels = new Dictionary<string, int>();

            foreach (var item in aNames)
            {
                List<voice> temp = voicesList.Where(x => x.person == item).ToList();
                labels.Add(item, temp.Count);
            }
            return labels.OrderBy(x => x.Value).Last().Key;
        }

        private List<voice> getVoices(List<VoiceData> aVoices, VoiceData aVoice)
        {
            List<voice> voicesList = new List<voice>();
            foreach (var voice in aVoices)
            {
                foreach (double[] vector in voice.Data)
                {
                    voicesList.Add(new voice(math.CityDistance(aVoice.getLast(), vector), voice.Name));
                }
            }
            return voicesList;
        }
        
        private List<voice> getOrderElements(List<voice> aVoices)
        {
            return aVoices.OrderBy(x => x.distance).Take(this.k).ToList();
        }
    }
}
