﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using static System.Console;

namespace Recognition
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Dla 10:");
            List<VoiceData> voices = Repository.getAll("10"); // 10 13 16
            var voice = Repository.getTestVoiceData10();
            classification(voices, voice, 1);
            classification(voices, voice, 3);
            classification(voices, voice, 5);
            WriteLine("***************************************");



            WriteLine("Dla 13:");
            voices = Repository.getAll("13"); // 10 13 16
            voice = Repository.getTestVoiceData13();
            classification(voices,voice,1);
            classification(voices, voice, 3);
            classification(voices, voice, 5);
            WriteLine("***************************************");

            WriteLine("Dla 16:");
            voices = Repository.getAll("16"); // 10 13 16
            voice = Repository.getTestVoiceData16();
            classification(voices, voice, 1);
            classification(voices, voice, 3);
            classification(voices, voice, 5);
            WriteLine("***************************************");
            WriteLine("***************************************");

            ReadKey();
        }

        static public List<String> getNames(List<VoiceData> aVoices)
        {
            List<String> names = new List<string>();

            foreach (var item in aVoices)
            {
                names.Add(item.Name);
            }
            return names;
        }

        static public double getNormalizationValue(List<VoiceData> aVoices)
        {
            double temp = 0.0;

            for (int i = 0; i < aVoices.Count; i++)
            {
                if (aVoices[i].getBiggestValue() > temp)
                {
                    temp = aVoices[i].getBiggestValue();
                }
            }
            return temp;
        }

        static public void getNormalized(List<VoiceData> aVoices, double aValue)
        {
            for (int i = 0; i < aVoices.Count; i++)
            {
                aVoices[i].Normalization(aValue);
            }
        }

        static public void classification(List<VoiceData> aVoices, VoiceData aVoice, int k)
        {
            Write("Dla k = " + k + " ");
            //Szukanie wartości do normalizacji
            aVoices.Add(aVoice);
            double normalizationValue = getNormalizationValue(aVoices);

            //Normalizacja 
            getNormalized(aVoices, normalizationValue);
            aVoices.RemoveAt(aVoices.Count - 1);

            //KNN
            KNN knn = new KNN(k);
            var response = knn.classification(aVoices, aVoice, getNames(aVoices));
            Console.WriteLine(response);
        }
    }
}
