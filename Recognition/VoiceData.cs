﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recognition
{


    public class VoiceData
    {
        private String name;
        private List<double[]> data;

        public string Name { get => name; set => name = value; }
        public List<double[]> Data { get => data; set => data = value; }

        public double[] getLast()
        {
            return data[data.Count - 1];
        }


        public VoiceData(string name, List<double[]> data)
        {
            this.name = name;
            this.data = data;
        }

        public double[] getVector(int aIndex)
        {
            return data[aIndex];
        }


        public double getBiggestValue()
        {
            var temp = 0.0;

            foreach(var item in data)
            {
                foreach (var secondItem in item)
                {
                    if (secondItem > temp)
                    {
                        temp = secondItem;
                    }
                }
            }
            return temp;
        }

        public void Normalization(double aLenght)
        {

            for (int i = 0; i < data.Count; i ++)
            {
                for (int j = 0; j < data[i].Length; j++)
                {
                    data[i][j] = data[i][j] / aLenght;
                }
            }
        }
    }
}
